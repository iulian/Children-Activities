from django import forms
from django.forms import ModelForm

from activities.models import Activity


class ActivityForm(ModelForm):
	class Meta:
		model = Activity
		fields = [
			'title',
			'activity_description',
			'activity_attachment',
			]
		localized_fields = '__all__'
		widgets = {
			'title': forms.TextInput(attrs = {'class': 'input is-medium', }),
			'activity_description': forms.Textarea(attrs = {'class': 'textarea is-medium'}),
			'activity_attachment': forms.FileInput(attrs = {'class': 'file-input'})
			}
