activities package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   activities.migrations

Submodules
----------

activities.admin module
-----------------------

.. automodule:: activities.admin
   :members:
   :undoc-members:
   :show-inheritance:

activities.apps module
----------------------

.. automodule:: activities.apps
   :members:
   :undoc-members:
   :show-inheritance:

activities.models module
------------------------

.. automodule:: activities.models
   :members:
   :undoc-members:
   :show-inheritance:

activities.tests module
-----------------------

.. automodule:: activities.tests
   :members:
   :undoc-members:
   :show-inheritance:

activities.urls module
----------------------

.. automodule:: activities.urls
   :members:
   :undoc-members:
   :show-inheritance:

activities.views module
-----------------------

.. automodule:: activities.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: activities
   :members:
   :undoc-members:
   :show-inheritance:
