from django.contrib import admin

from corporate_pages.models import CorporatePage, ContactPage

# Register your models here.

admin.site.register(CorporatePage)
admin.site.register(ContactPage)
