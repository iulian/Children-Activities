from django.apps import AppConfig


class CorporatePagesConfig(AppConfig):
    name = 'corporate_pages'
